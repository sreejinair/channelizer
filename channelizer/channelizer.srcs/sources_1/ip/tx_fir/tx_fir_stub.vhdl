-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Tue Jun 30 15:02:31 2020
-- Host        : DESKTOP-KCJH564 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/Users/sreej/Documents/vivado/channelizer/channelizer.srcs/sources_1/ip/tx_fir/tx_fir_stub.vhdl
-- Design      : tx_fir
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7vx485tffg1157-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tx_fir is
  Port ( 
    aclk : in STD_LOGIC;
    s_axis_data_tvalid : in STD_LOGIC;
    s_axis_data_tready : out STD_LOGIC;
    s_axis_data_tdata : in STD_LOGIC_VECTOR ( 47 downto 0 );
    s_axis_config_tvalid : in STD_LOGIC;
    s_axis_config_tready : out STD_LOGIC;
    s_axis_config_tlast : in STD_LOGIC;
    s_axis_config_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axis_data_tvalid : out STD_LOGIC;
    m_axis_data_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    event_s_config_tlast_missing : out STD_LOGIC;
    event_s_config_tlast_unexpected : out STD_LOGIC
  );

end tx_fir;

architecture stub of tx_fir is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "aclk,s_axis_data_tvalid,s_axis_data_tready,s_axis_data_tdata[47:0],s_axis_config_tvalid,s_axis_config_tready,s_axis_config_tlast,s_axis_config_tdata[7:0],m_axis_data_tvalid,m_axis_data_tdata[47:0],event_s_config_tlast_missing,event_s_config_tlast_unexpected";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "fir_compiler_v7_2_10,Vivado 2017.4";
begin
end;
