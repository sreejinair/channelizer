// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Tue Jun 30 15:02:31 2020
// Host        : DESKTOP-KCJH564 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/Users/sreej/Documents/vivado/channelizer/channelizer.srcs/sources_1/ip/tx_fir/tx_fir_stub.v
// Design      : tx_fir
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7vx485tffg1157-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fir_compiler_v7_2_10,Vivado 2017.4" *)
module tx_fir(aclk, s_axis_data_tvalid, s_axis_data_tready, 
  s_axis_data_tdata, s_axis_config_tvalid, s_axis_config_tready, s_axis_config_tlast, 
  s_axis_config_tdata, m_axis_data_tvalid, m_axis_data_tdata, 
  event_s_config_tlast_missing, event_s_config_tlast_unexpected)
/* synthesis syn_black_box black_box_pad_pin="aclk,s_axis_data_tvalid,s_axis_data_tready,s_axis_data_tdata[47:0],s_axis_config_tvalid,s_axis_config_tready,s_axis_config_tlast,s_axis_config_tdata[7:0],m_axis_data_tvalid,m_axis_data_tdata[47:0],event_s_config_tlast_missing,event_s_config_tlast_unexpected" */;
  input aclk;
  input s_axis_data_tvalid;
  output s_axis_data_tready;
  input [47:0]s_axis_data_tdata;
  input s_axis_config_tvalid;
  output s_axis_config_tready;
  input s_axis_config_tlast;
  input [7:0]s_axis_config_tdata;
  output m_axis_data_tvalid;
  output [47:0]m_axis_data_tdata;
  output event_s_config_tlast_missing;
  output event_s_config_tlast_unexpected;
endmodule
