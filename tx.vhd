-- (c) Copyright 2012 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-------------------------------------------------------------------------------
-- Description:
--  Implements a 8-channel poly-phase channelizer transmitter
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity tx is
  port ( 
    aclk                 : in  STD_LOGIC;
    s_axis_data_tdata    : in  STD_LOGIC_VECTOR (31 downto 0);
    s_axis_data_tvalid   : in  STD_LOGIC;
    s_axis_data_tready   : out STD_LOGIC;
    
    --pragma translate off
    debug_fft_tvalid     : out STD_LOGIC;
    debug_fft_tready     : out STD_LOGIC;
    debug_fft_real       : out STD_LOGIC_VECTOR (19 downto 0);
    debug_fft_imag       : out STD_LOGIC_VECTOR (19 downto 0);
    --pragma translate on
    
    real_out             : out STD_LOGIC_VECTOR (18 downto 0);
    imag_out             : out STD_LOGIC_VECTOR (18 downto 0);
    valid_out            : out STD_LOGIC
    );
end tx;

architecture top_level of tx is
  -- IFFT block
  COMPONENT tx_fft
  PORT (
    aclk : IN STD_LOGIC;
    s_axis_config_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_config_tvalid : IN STD_LOGIC;
    s_axis_config_tready : OUT STD_LOGIC;
    s_axis_data_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axis_data_tvalid : IN STD_LOGIC;
    s_axis_data_tready : OUT STD_LOGIC;
    s_axis_data_tlast : IN STD_LOGIC;
    m_axis_data_tdata : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    m_axis_data_tvalid : OUT STD_LOGIC;
    m_axis_data_tready : IN STD_LOGIC;
    m_axis_data_tlast : OUT STD_LOGIC;
    event_frame_started : OUT STD_LOGIC;
    event_tlast_unexpected : OUT STD_LOGIC;
    event_tlast_missing : OUT STD_LOGIC;
    event_status_channel_halt : OUT STD_LOGIC;
    event_data_in_channel_halt : OUT STD_LOGIC;
    event_data_out_channel_halt : OUT STD_LOGIC
  );
  END COMPONENT;
  
  -- FIFO to buffer block output of the FFT before the sample based input of the FIR
  COMPONENT tx_fifo
  PORT (
    s_aclk : IN STD_LOGIC;
    s_aresetn : IN STD_LOGIC;
    s_axis_tvalid : IN STD_LOGIC;
    s_axis_tready : OUT STD_LOGIC;
    s_axis_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_tvalid : OUT STD_LOGIC;
    m_axis_tready : IN STD_LOGIC;
    m_axis_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
  );
  END COMPONENT;
  
  -- FIR block
  COMPONENT tx_fir
  PORT (
    aclk : IN STD_LOGIC;
    s_axis_data_tvalid : IN STD_LOGIC;
    s_axis_data_tready : OUT STD_LOGIC;
    s_axis_data_tdata : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    s_axis_config_tvalid : IN STD_LOGIC;
    s_axis_config_tready : OUT STD_LOGIC;
    s_axis_config_tlast : IN STD_LOGIC;
    s_axis_config_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_data_tvalid : OUT STD_LOGIC;
    m_axis_data_tdata : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    event_s_config_tlast_missing : OUT STD_LOGIC;
    event_s_config_tlast_unexpected : OUT STD_LOGIC
  );
  END COMPONENT;
  
  signal s_fifo_tdata,
         m_fifo_tdata       : std_logic_vector(63 downto 0):=(others=>'0');
  signal s_fir_tdata,
         m_fft_tdata,
         m_fir_tdata        : std_logic_vector(47 downto 0):=(others=>'0');
  signal s_fir_config_tdata : std_logic_vector(7 downto 0):=(others=>'0');
  signal m_fft_tvalid, 
         m_fft_tready, 
         m_fifo_tvalid,
         m_fifo_tready,
         s_fir_config_tready,
         s_fir_config_tvalid,
         fir_config_complete : std_logic := '0';
         
begin
  
  i_fft : tx_fft
  PORT MAP (
    aclk => aclk,
    
    s_axis_config_tdata => "00000000", -- FWD/INV(bit 0) 0 = Inverse FFT
    s_axis_config_tvalid => '1',
    s_axis_config_tready => open,
    
    s_axis_data_tdata  => s_axis_data_tdata,
    s_axis_data_tvalid => s_axis_data_tvalid,
    s_axis_data_tready => s_axis_data_tready,
    s_axis_data_tlast  => '0', -- ignore event
    
    m_axis_data_tdata => m_fft_tdata,
    m_axis_data_tvalid => m_fft_tvalid,
    m_axis_data_tready => m_fft_tready,
    m_axis_data_tlast => open,
    
    event_frame_started => open,
    event_tlast_unexpected => open,
    event_tlast_missing => open,
    event_status_channel_halt => open,
    event_data_in_channel_halt => open,
    event_data_out_channel_halt => open
  );
  
  s_fifo_tdata(17 downto 0)  <= m_fft_tdata(18 downto 1);
  s_fifo_tdata(35 downto 18) <= m_fft_tdata(42 downto 25);
  
  -- Connect debug ports
  --pragma translate off
  debug_fft_tvalid <= m_fft_tvalid;
  debug_fft_tready <= m_fft_tready;
  debug_fft_real   <= m_fft_tdata(19 downto 0);
  debug_fft_imag   <= m_fft_tdata(43 downto 24);
  --pragma translate on
  
  -- The FIFO is used to buffer the data between the FFT and FIR
  --  o The FFT will produce a burst of data where as FIR will consume the samples at a continuous slower rate. The average
  --  of both cores is the same and the FIFO should never become full.
  i_fifo : tx_fifo
  PORT MAP (
    s_aclk => aclk,
    s_aresetn => '1',
    
    s_axis_tvalid => m_fft_tvalid,
    s_axis_tready => m_fft_tready, -- Should never fill i.e. this signal should never de-assert
    s_axis_tdata  => s_fifo_tdata,
    
    m_axis_tvalid => m_fifo_tvalid,
    m_axis_tready => m_fifo_tready,
    m_axis_tdata =>  m_fifo_tdata
  );
  
  -- Startup configuration for the FIR
  --  o Selects which coefficient set to use for which interleaved channel
  i_startup_config: process(aclk)
  begin
    if (rising_edge(aclk)) then
      if s_fir_config_tready = '1' then
        if s_fir_config_tvalid = '1' then
          s_fir_config_tdata <= std_logic_vector(unsigned(s_fir_config_tdata) + 1);
          if unsigned(s_fir_config_tdata) = 6 then
            fir_config_complete <= '1';
          end if;
        end if;
        s_fir_config_tvalid <= not fir_config_complete;
      end if;
    end if;
  end process;
  
  s_fir_tdata(17 downto 0)  <= m_fifo_tdata(17 downto 0);
  s_fir_tdata(41 downto 24) <= m_fifo_tdata(35 downto 18);
  
  i_fir : tx_fir
  PORT MAP (
    aclk => aclk,
    
    s_axis_data_tvalid => m_fifo_tvalid,
    s_axis_data_tready => m_fifo_tready,
    s_axis_data_tdata    => s_fir_tdata,
    
    s_axis_config_tvalid => s_fir_config_tvalid,
    s_axis_config_tready => s_fir_config_tready,
    s_axis_config_tlast  => '0',  -- Ignore event. The lack of a TLAST pulse will generate an event on 
                                  -- event_s_config_tlast_missing but it does not affect core operation
    s_axis_config_tdata  => s_fir_config_tdata,
    
    m_axis_data_tvalid   => valid_out,
    m_axis_data_tdata    => m_fir_tdata,
    
    event_s_config_tlast_missing    => open,
    event_s_config_tlast_unexpected => open
  );
  
  real_out <= m_fir_tdata(18 downto 0); 
  imag_out <= m_fir_tdata(42 downto 24);
  
end top_level;

