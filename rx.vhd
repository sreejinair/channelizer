-- (c) Copyright 2012 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-------------------------------------------------------------------------------
-- Description:
--  Implements a 8-channel poly-phase channelizer receiver
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity rx is
  port ( 
    aclk               : in  STD_LOGIC;
    real_in            : in  STD_LOGIC_VECTOR (15 downto 0);
    imag_in            : in  STD_LOGIC_VECTOR (15 downto 0);
    valid_in           : in  STD_LOGIC;
    
    --pragma translate off
    debug_fir_real     : out STD_LOGIC_VECTOR(18 downto 0);
    debug_fir_imag     : out STD_LOGIC_VECTOR(18 downto 0);
    debug_fir_valid    : out STD_LOGIC;
    --pragma translate on
    
    m_axis_data_tdata  : out  STD_LOGIC_VECTOR (47 downto 0);
    m_axis_data_tvalid : out  STD_LOGIC;
    m_axis_data_tready : in   STD_LOGIC);
end rx;

architecture top_level of rx is
  
  -- FIR block
-- COMPONENT rx_fir
 -- PORT (
--   aclk : IN STD_LOGIC;
  --  s_axis_data_tvalid : IN STD_LOGIC;
 --   s_axis_data_tready : OUT STD_LOGIC;
  --  s_axis_data_tlast : IN STD_LOGIC;
  --  s_axis_data_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
 --   s_axis_config_tvalid : IN STD_LOGIC;
--    s_axis_config_tready : OUT STD_LOGIC;
  -- X s_axis_config_tlast : IN STD_LOGIC;
  --  s_axis_config_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
 --   m_axis_data_tvalid : OUT STD_LOGIC;
--    m_axis_data_tlast : OUT STD_LOGIC;
 --   m_axis_data_tuser : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
 --   m_axis_data_tdata : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
--    event_s_data_tlast_missing : OUT STD_LOGIC;
--    event_s_data_tlast_unexpected : OUT STD_LOGIC;
--    event_s_config_tlast_missing : OUT STD_LOGIC;
--    event_s_config_tlast_unexpected : OUT STD_LOGIC
--  );
-- END COMPONENT;
  
  COMPONENT rx_fir
    PORT (
      aclk : IN STD_LOGIC;
      s_axis_data_tvalid : IN STD_LOGIC;
      s_axis_data_tready : OUT STD_LOGIC;
      s_axis_data_tlast : IN STD_LOGIC;
      s_axis_data_tuser : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      s_axis_data_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axis_config_tvalid : IN STD_LOGIC;
      s_axis_config_tready : OUT STD_LOGIC;
      s_axis_config_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      m_axis_data_tvalid : OUT STD_LOGIC;
      m_axis_data_tready : IN STD_LOGIC;
      m_axis_data_tlast : OUT STD_LOGIC;
      m_axis_data_tuser : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      m_axis_data_tdata : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
      event_s_data_tlast_missing : OUT STD_LOGIC;
      event_s_data_tlast_unexpected : OUT STD_LOGIC
    );
  END COMPONENT; -- New IP Added Version 7.2
  
  -- Memory to buffer the sample based output of the FIR and flip the channel order before the block based input of the FFT 
  --  o The receiver requires a different commutator direction to the transmitter and this implmented by flipping the 
  --    channel order using this buffer
  COMPONENT rx_mem
  PORT (
    a : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    d : IN STD_LOGIC_VECTOR(33 DOWNTO 0);
    dpra : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    clk : IN STD_LOGIC;
    we : IN STD_LOGIC;
    qdpo_clk : IN STD_LOGIC;
    qdpo : OUT STD_LOGIC_VECTOR(33 DOWNTO 0)
  );
  END COMPONENT;
  
  -- Read address counter to reverse the channel order
  COMPONENT rx_reverse_addr
  PORT (
    clk : IN STD_LOGIC;
    ce : IN STD_LOGIC;
    load : IN STD_LOGIC;
    l : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    q : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
  END COMPONENT;

  -- IFFT block
--  COMPONENT rx_fft
--  PORT (
--    aclk : IN STD_LOGIC;
--    s_axis_config_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--    s_axis_config_tvalid : IN STD_LOGIC;
--    s_axis_config_tready : OUT STD_LOGIC;
--    s_axis_data_tdata : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
--    s_axis_data_tvalid : IN STD_LOGIC;
--    s_axis_data_tready : OUT STD_LOGIC;
--    s_axis_data_tlast : IN STD_LOGIC;
--    m_axis_data_tdata : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
--    m_axis_data_tvalid : OUT STD_LOGIC;
--    m_axis_data_tready : IN STD_LOGIC;
--    m_axis_data_tlast : OUT STD_LOGIC;
--    event_frame_started : OUT STD_LOGIC;
--    event_tlast_unexpected : OUT STD_LOGIC;
--    event_tlast_missing : OUT STD_LOGIC;
--    event_status_channel_halt : OUT STD_LOGIC;
--    event_data_in_channel_halt : OUT STD_LOGIC;
--    event_data_out_channel_halt : OUT STD_LOGIC
--  );
--  END COMPONENT;
  COMPONENT rx_fft
    PORT (
      aclk : IN STD_LOGIC;
      s_axis_config_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      s_axis_config_tvalid : IN STD_LOGIC;
      s_axis_config_tready : OUT STD_LOGIC;
      s_axis_data_tdata : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
      s_axis_data_tvalid : IN STD_LOGIC;
      s_axis_data_tready : OUT STD_LOGIC;
      s_axis_data_tlast : IN STD_LOGIC;
      m_axis_data_tdata : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
      m_axis_data_tvalid : OUT STD_LOGIC;
      m_axis_data_tready : IN STD_LOGIC;
      m_axis_data_tlast : OUT STD_LOGIC;
      event_frame_started : OUT STD_LOGIC;
      event_tlast_unexpected : OUT STD_LOGIC;
      event_tlast_missing : OUT STD_LOGIC;
      event_status_channel_halt : OUT STD_LOGIC;
      event_data_in_channel_halt : OUT STD_LOGIC;
      event_data_out_channel_halt : OUT STD_LOGIC
    );
  END COMPONENT;
  
  
  -- Output FIFO to spread out the block based output of the FFT
--  COMPONENT rx_fifo
--  PORT (
--    s_aclk : IN STD_LOGIC;
--    s_aresetn : IN STD_LOGIC;
--    s_axis_tvalid : IN STD_LOGIC;
--    s_axis_tready : OUT STD_LOGIC;
--    s_axis_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
--    m_axis_tvalid : OUT STD_LOGIC;
--    m_axis_tready : IN STD_LOGIC;
--    m_axis_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
--  );
--  END COMPONENT;
  COMPONENT rx_fifo
    PORT (
      wr_rst_busy : OUT STD_LOGIC;
      rd_rst_busy : OUT STD_LOGIC;
      s_aclk : IN STD_LOGIC;
      s_aresetn : IN STD_LOGIC;
      s_axis_tvalid : IN STD_LOGIC;
      s_axis_tready : OUT STD_LOGIC;
      s_axis_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
      m_axis_tvalid : OUT STD_LOGIC;
      m_axis_tready : IN STD_LOGIC;
      m_axis_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
    );
  END COMPONENT;
  
  signal s_fir_config_tdata  : std_logic_vector(7 downto 0):=std_logic_vector(to_unsigned(7,8));
  signal s_fir_tdata         : std_logic_vector(31 downto 0):=(others=>'0');
  signal m_fir_tdata         : std_logic_vector(47 downto 0):=(others=>'0');
  signal m_fir_chanid        : std_logic_vector(2 downto 0):=(others=>'0');
  signal mem_din,
         mem_dout            : std_logic_vector(33 downto 0):=(others=>'0');
  signal mem_wr_addr,
         mem_rd_addr         : std_logic_vector(3 downto 0):=(others=>'0');
  signal s_fft_read_addr     : std_logic_vector(2 downto 0):=(others=>'0');
  signal s_fft_tdata,
         m_fft_tdata         : std_logic_vector(47 downto 0):=(others=>'0');
  signal s_fifo_tdata,
         m_fifo_tdata        : std_logic_vector(63 downto 0):=(others=>'0');
  signal s_fir_config_tvalid,
         s_fir_config_tready,
         fir_config_complete,
         m_fir_tvalid,
         m_fir_tlast,
         m_fir_page,
         s_fft_tvalid,
         s_fft_tvalid_align,
         s_fft_start,
         s_fft_tready,
         m_fft_tvalid,
         m_fft_tready,
         reverse_addr_enable
         : std_logic := '0';
  signal s_fft_page : std_logic := '1';
  
begin
  
  -- Startup configuration for the FIR
  --  o Selects which coefficient set to use for which interleaved channel
  i_startup_config: process(aclk)
  begin
    if (rising_edge(aclk)) then
      if s_fir_config_tready = '1' then
        if s_fir_config_tvalid = '1' then
          s_fir_config_tdata <= std_logic_vector(unsigned(s_fir_config_tdata) - 1);
          if unsigned(s_fir_config_tdata) = 1 then
            fir_config_complete <= '1';
          end if;
        end if;
        s_fir_config_tvalid <= not fir_config_complete;
      end if;
    end if;
  end process;
  
  s_fir_tdata <= imag_in & real_in;
  
  i_fir: rx_fir
  PORT MAP (
    aclk => aclk,
    s_axis_data_tvalid => valid_in,
    s_axis_data_tready => open,
    s_axis_data_tuser  => "000", -- added for ip version 7.2
    s_axis_data_tlast  => '0', -- Ignore event
    s_axis_data_tdata  => s_fir_tdata,
    s_axis_config_tvalid  => s_fir_config_tvalid,
    s_axis_config_tready  => s_fir_config_tready,
    m_axis_data_tready=>'0', -- added new port and assigned '0'
    --s_axis_config_tlast   => '0', 
    s_axis_config_tdata   => s_fir_config_tdata,
    m_axis_data_tvalid => m_fir_tvalid,
    m_axis_data_tlast  => m_fir_tlast,
    m_axis_data_tuser  => m_fir_chanid,
    m_axis_data_tdata  => m_fir_tdata,
    
    event_s_data_tlast_missing => open,
    event_s_data_tlast_unexpected => open
   -- event_s_config_tlast_missing => open, --removed as these signals are not available 7.2
   -- event_s_config_tlast_unexpected => open --removed as these signals are not available 7.2
  );
  
  --pragma translate off
  debug_fir_real  <= m_fir_tdata(18 downto 0);
  debug_fir_imag  <= m_fir_tdata(42 downto 24);
  debug_fir_valid <= m_fir_tvalid;
  --pragma translate on
  
  i_mem_page: process(aclk)
  begin
    if (rising_edge(aclk)) then
      if m_fir_tlast='1' and m_fir_tvalid='1' then
        m_fir_page <= not m_fir_page;
        s_fft_page <= not s_fft_page;
      end if;
    end if;
  end process;
  
  mem_din     <= m_fir_tdata(42 downto 26) & m_fir_tdata(18 downto 2);
  mem_wr_addr <= m_fir_page & m_fir_chanid;
  mem_rd_addr <= s_fft_page & s_fft_read_addr;
  
  i_reverse_mem: rx_mem
  PORT MAP (
    a    => mem_wr_addr,
    d    => mem_din,
    we   => m_fir_tvalid,
    clk  => aclk,
    
    dpra => mem_rd_addr,
    qdpo_clk => aclk,
    qdpo     => mem_dout
  );
  
  s_fft_tdata(16 downto 0)  <= mem_dout(16 downto 0);
  s_fft_tdata(40 downto 24) <= mem_dout(33 downto 17);
  
  s_fft_start         <= m_fir_tvalid and m_fir_tlast;
  -- Places a LUT on the CE port, can be a slow path
  reverse_addr_enable <= s_fft_start or s_fft_tvalid;
  
  i_reverse_addr: rx_reverse_addr
  PORT MAP (
    clk           => aclk,
    ce            => reverse_addr_enable,
    load          => s_fft_start,
    l             => "1111",
    q(2 downto 0) => s_fft_read_addr,
    q(3)          => s_fft_tvalid
  );
  
  i_fft_tvalid_align: process(aclk)
  begin
    if (rising_edge(aclk)) then
      s_fft_tvalid_align <= s_fft_tvalid;
    end if;
  end process;
  
  i_fft: rx_fft
  PORT MAP (
    aclk => aclk,
    s_axis_config_tdata => "0000000000000000", -- FWD/INV(bit 0) 0 = Inverse FFT
    s_axis_config_tvalid => s_fft_tready,--'1',
    s_axis_config_tready => s_fft_tready,    
    s_axis_data_tdata => s_fft_tdata,
    s_axis_data_tvalid => s_fft_tvalid_align,
    s_axis_data_tready => open,
    s_axis_data_tlast => '0', -- Ignore event
    m_axis_data_tdata => m_fft_tdata,
    m_axis_data_tvalid => m_fft_tvalid,
    m_axis_data_tready => m_fft_tready,
    m_axis_data_tlast => open,
    event_frame_started => open,
    event_tlast_unexpected => open,
    event_tlast_missing => open,
    event_status_channel_halt => open,
    event_data_in_channel_halt => open,
    event_data_out_channel_halt => open
  );
  
  s_fifo_tdata(47 downto 0) <= m_fft_tdata;
  
  i_fifo: rx_fifo
  PORT MAP (
  wr_rst_busy =>open,
        rd_rst_busy =>open,
    s_aclk        => aclk,
    s_aresetn     => '1',
                  
    s_axis_tvalid => m_fft_tvalid,
    s_axis_tready => m_fft_tready,
    s_axis_tdata  => s_fifo_tdata,
                  
    m_axis_tvalid => m_axis_data_tvalid, 
    m_axis_tready => m_axis_data_tready,
    m_axis_tdata  => m_fifo_tdata
  );
  m_axis_data_tdata <= m_fifo_tdata(47 downto 0);
  
end top_level;

