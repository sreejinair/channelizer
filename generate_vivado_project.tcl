# Generates an Vivado project implementing a poly-phase channelizer transmit and recieve path.

set script_name  "GENERATE_VIVADO_PROJECT.TCL"
set proj_name    "channelizer"
set appnote_root [file normalize "[pwd]/.."]
puts "$script_name: APPNOTE_ROOT will be set to $appnote_root"

###############################################################################
# Create new project and setup properties
puts "$script_name: Create project..."
create_project $proj_name [file join $appnote_root vivado $proj_name] -part xc7vx485tffg1157-2 -force
set_property target_language VHDL [current_project]

###############################################################################
# Add the source files
puts "$script_name: Adding sources to project..."
add_files -norecurse [file join $appnote_root hdl rx.vhd]
add_files -norecurse [file join $appnote_root hdl tx.vhd]

add_files -fileset sim_1 -norecurse [file join $appnote_root hdl read_data_file.vhd]
add_files -fileset sim_1 -norecurse [file join $appnote_root hdl testbench.vhd]
set_property used_in_synthesis false [get_files [file join $appnote_root hdl read_data_file.vhd]]
set_property used_in_synthesis false [get_files [file join $appnote_root hdl testbench.vhd]]

###############################################################################
# Add/Create Transmit path IP cores
create_ip -name fir_compiler -version 7.2 -vendor xilinx.com -library ip -module_name tx_fir
#set_property -name CONFIG.component_name               -value {tx_fir} -objects [get_ips tx_fir]
#set_property -name CONFIG.coefficientsource            -value {COE_File} -objects [get_ips tx_fir]
#set_property -name CONFIG.coefficient_file             -value [file join $appnote_root coefficients channelizer_8.coe] -objects [get_ips tx_fir]
set_property -dict [list CONFIG.CoefficientSource {COE_File} CONFIG.Coefficient_File [file join $appnote_root coefficients channelizer_8.coe]] [get_ips tx_fir]
set_property -name CONFIG.coefficient_sets             -value {8} -objects [get_ips tx_fir]
set_property -name CONFIG.coefficient_width            -value {25} -objects [get_ips tx_fir]
set_property -name CONFIG.coefficient_fractional_bits  -value {27} -objects [get_ips tx_fir]
set_property -name CONFIG.data_width                   -value {18} -objects [get_ips tx_fir]
set_property -name CONFIG.data_fractional_bits         -value {17} -objects [get_ips tx_fir]
set_property -name CONFIG.filter_type                  -value {Single_Rate} -objects [get_ips tx_fir]
set_property -name CONFIG.number_channels              -value {8} -objects [get_ips tx_fir]
set_property -name CONFIG.number_paths                 -value {2} -objects [get_ips tx_fir]
set_property -name CONFIG.output_rounding_mode         -value {Convergent_Rounding_to_Even} -objects [get_ips tx_fir]
set_property -name CONFIG.output_width                 -value {19} -objects [get_ips tx_fir]
set_property -name CONFIG.ratespecification            -value {Input_Sample_Period} -objects [get_ips tx_fir]
set_property -name CONFIG.sampleperiod                 -value {89} -objects [get_ips tx_fir]
set_property -name CONFIG.s_data_has_fifo              -value {false} -objects [get_ips tx_fir]
set_property -name CONFIG.s_config_method              -value {By_Channel} -objects [get_ips tx_fir]
set_property -name CONFIG.s_config_sync_mode           -value {On_Vector} -objects [get_ips tx_fir]

create_ip -name xfft -version 9.0 -vendor xilinx.com -library ip -module_name tx_fft
set_property -name CONFIG.component_name          -value {tx_fft} -objects [get_ips tx_fft]
set_property -name CONFIG.implementation_options  -value {radix_2_lite_burst_io} -objects [get_ips tx_fft]
set_property -name CONFIG.output_ordering         -value {natural_order} -objects [get_ips tx_fft]
set_property -name CONFIG.butterfly_type          -value {use_xtremedsp_slices} -objects [get_ips tx_fft]
set_property -name CONFIG.transform_length        -value {8} -objects [get_ips tx_fft]
set_property -name CONFIG.input_width             -value {16} -objects [get_ips tx_fft]
set_property -name CONFIG.phase_factor_width      -value {17} -objects [get_ips tx_fft]
set_property -name CONFIG.rounding_modes          -value {convergent_rounding} -objects [get_ips tx_fft]
set_property -name CONFIG.scaling_options         -value {unscaled} -objects [get_ips tx_fft]
set_property -name CONFIG.throttle_scheme         -value {nonrealtime} -objects [get_ips tx_fft]

create_ip -name fifo_generator -version 13.2 -vendor xilinx.com -library ip -module_name tx_fifo
set_property -name CONFIG.Component_Name            -value {tx_fifo} -objects [get_ips tx_fifo]
set_property -name CONFIG.Interface_Type            -value {AXI_STREAM} -objects [get_ips tx_fifo]
set_property -name CONFIG.FIFO_Implementation_axis  -value {Common_Clock_Distributed_RAM} -objects [get_ips tx_fifo]
set_property -name CONFIG.Input_Depth_axis          -value {16} -objects [get_ips tx_fifo]

###############################################################################
# Add/Create Recieve path IP cores
create_ip -name fir_compiler -version 7.2 -vendor xilinx.com -library ip -module_name rx_fir
set_property -name CONFIG.component_name               -value {rx_fir} -objects [get_ips rx_fir]
#set_property -name CONFIG.coefficientsource            -value {COE_File} -objects [get_ips rx_fir]
#set_property -name CONFIG.coefficient_file             -value [file join $appnote_root coefficients channelizer_8.coe] -objects [get_ips rx_fir]
set_property -dict [list CONFIG.CoefficientSource {COE_File} CONFIG.Coefficient_File [file join $appnote_root coefficients channelizer_8.coe]] [get_ips rx_fir]
set_property -name CONFIG.coefficient_sets             -value {8} -objects [get_ips rx_fir]
set_property -name CONFIG.coefficient_width            -value {25} -objects [get_ips rx_fir]
set_property -name CONFIG.coefficient_fractional_bits  -value {27} -objects [get_ips rx_fir]
set_property -name CONFIG.data_width                   -value {16} -objects [get_ips rx_fir]
set_property -name CONFIG.data_fractional_bits         -value {15} -objects [get_ips rx_fir]
set_property -name CONFIG.filter_type                  -value {Single_Rate} -objects [get_ips rx_fir]
set_property -name CONFIG.number_channels              -value {8} -objects [get_ips rx_fir]
set_property -name CONFIG.number_paths                 -value {2} -objects [get_ips rx_fir]
set_property -name CONFIG.output_rounding_mode         -value {Convergent_Rounding_to_Even} -objects [get_ips rx_fir]
set_property -name CONFIG.output_width                 -value {19} -objects [get_ips rx_fir]
set_property -name CONFIG.ratespecification            -value {Input_Sample_Period} -objects [get_ips rx_fir]
set_property -name CONFIG.sampleperiod                 -value {89} -objects [get_ips rx_fir]
set_property -name CONFIG.s_data_has_fifo              -value {false} -objects [get_ips rx_fir]
set_property -name CONFIG.s_config_method              -value {By_Channel} -objects [get_ips rx_fir]
set_property -name CONFIG.s_config_sync_mode           -value {On_Vector} -objects [get_ips rx_fir]
set_property -name CONFIG.data_has_tlast               -value {Vector_Framing} -objects [get_ips rx_fir]
set_property -name CONFIG.m_data_has_tuser             -value {Chan_ID_Field} -objects [get_ips rx_fir]

create_ip -name xfft -version 9.0 -vendor xilinx.com -library ip -module_name rx_fft
set_property -name CONFIG.component_name          -value {rx_fft} -objects [get_ips rx_fft]
set_property -name CONFIG.implementation_options  -value {radix_2_lite_burst_io} -objects [get_ips rx_fft]
set_property -name CONFIG.output_ordering         -value {natural_order} -objects [get_ips rx_fft]
set_property -name CONFIG.butterfly_type          -value {use_xtremedsp_slices} -objects [get_ips rx_fft]
set_property -name CONFIG.transform_length        -value {8} -objects [get_ips rx_fft]
set_property -name CONFIG.input_width             -value {17} -objects [get_ips rx_fft]
set_property -name CONFIG.phase_factor_width      -value {17} -objects [get_ips rx_fft]
set_property -name CONFIG.rounding_modes          -value {convergent_rounding} -objects [get_ips rx_fft]
set_property -name CONFIG.scaling_options         -value {unscaled} -objects [get_ips rx_fft]
set_property -name CONFIG.throttle_scheme         -value {nonrealtime} -objects [get_ips rx_fft]

create_ip -name c_counter_binary -version 12.0 -vendor xilinx.com -library ip -module_name rx_reverse_addr
set_property -name CONFIG.Component_Name    -value {rx_reverse_addr} -objects [get_ips rx_reverse_addr]
set_property -name CONFIG.Output_Width      -value {4} -objects [get_ips rx_reverse_addr]
set_property -name CONFIG.Restrict_Count    -value {true} -objects [get_ips rx_reverse_addr]
set_property -name CONFIG.Final_Count_Value -value {7} -objects [get_ips rx_reverse_addr]
set_property -name CONFIG.Count_Mode        -value {DOWN} -objects [get_ips rx_reverse_addr]
set_property -name CONFIG.CE                -value {true} -objects [get_ips rx_reverse_addr]
set_property -name CONFIG.Load              -value {true} -objects [get_ips rx_reverse_addr]

create_ip -name dist_mem_gen -version 8.0 -vendor xilinx.com -library ip -module_name rx_mem
set_property -name CONFIG.Component_Name  -value {rx_mem} -objects [get_ips rx_mem]
set_property -name CONFIG.depth           -value {16} -objects [get_ips rx_mem]
set_property -name CONFIG.data_width      -value {34} -objects [get_ips rx_mem]
set_property -name CONFIG.memory_type     -value {simple_dual_port_ram} -objects [get_ips rx_mem]
set_property -name CONFIG.output_options  -value {registered} -objects [get_ips rx_mem]

create_ip -name fifo_generator -version 13.2 -vendor xilinx.com -library ip -module_name rx_fifo
set_property -name CONFIG.Component_Name            -value {rx_fifo} -objects [get_ips rx_fifo]
set_property -name CONFIG.Interface_Type            -value {AXI_STREAM} -objects [get_ips rx_fifo]
set_property -name CONFIG.FIFO_Implementation_axis  -value {Common_Clock_Distributed_RAM} -objects [get_ips rx_fifo]
set_property -name CONFIG.Input_Depth_axis          -value {16} -objects [get_ips rx_fifo]

###############################################################################
# Generate IP targets
puts "$script_name: Generating core output products..."
generate_target {instantiation_template synthesis simulation} [get_ips] -force

###############################################################################
# Setup testbench APPNOTE_ROOT generic
set_property generic APPNOTE_ROOT_DIR=$appnote_root [get_filesets sim_1]

###############################################################################
# Set initial project top module
set_property top tx [current_fileset]
set_property top_file [list $appnote_root hdl tx.vhd] [current_fileset]
update_compile_order -fileset sources_1

puts "$script_name: Complete!!"


